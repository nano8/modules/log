<?php

use laylatichy\nano\modules\log\Log;
use laylatichy\nano\modules\log\LogModule;
use Monolog\Handler\Handler;

if (!function_exists('defineLogHandler')) {
    function defineLogHandler(Handler $handler): void {
        useNanoModule(LogModule::class)
            ->log
            ->addHandler($handler);
    }
}

if (!function_exists('useLog')) {
    function useLog(): Log {
        return useNanoModule(LogModule::class)
            ->log;
    }
}