<?php

namespace laylatichy\nano\modules\log;

use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

class LogModule implements NanoModule {
    public Log $log;

    public function __construct(
        private readonly string $name = 'nano',
    ) {}

    public function register(Nano $nano): void {
        if (isset($this->log)) {
            useNanoException('log module already registered');
        }

        $this->log = new Log($this->name);

        $this->load();
    }

    private function load(): void {
        $file = useConfig()->getRoot() . '/../.config/log.php';

        if (file_exists($file)) {
            require $file;
        }
    }
}
