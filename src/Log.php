<?php

namespace laylatichy\nano\modules\log;

use Exception;
use Monolog\Handler\Handler;
use Monolog\Logger;
use Throwable;

class Log {
    public Logger $logger;

    public function __construct(string $name) {
        $this->logger = new Logger($name);
    }

    public function addHandler(Handler $handler): void {
        $this->logger->pushHandler($handler);
    }

    public function info(string $message, array $data = []): void {
        $this->logger->info($message, $data);
    }

    public function debug(string $message, array $data = []): void {
        $this->logger->debug($message, $data);
    }

    public function notice(string $message, array $data = []): void {
        $this->logger->notice($message, $data);
    }

    public function warning(string $message, array $data = []): void {
        $this->logger->warning($message, $data);
    }

    public function error(string $message, Exception|Throwable $e, array $data = []): void {
        $this->logger->error($message, [
            'trace' => [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'data' => $e->getTrace(),
            ],
            'error' => $e->getMessage(),
            'data'  => $data,
        ]);
    }
}
