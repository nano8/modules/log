---
layout: doc
---

# installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-log
```

# registering module

```php
use laylatichy\nano\modules\log\LogModule;

useNano()->withModule(new LogModule('app'));

// define your handlers here or in .config/log.php file
```
