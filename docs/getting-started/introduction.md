---
layout: doc
---

# introduction

## what is nano/modules/log?

`nano/modules/log` is a simple wrapper around [monolog](https://seldaek.github.io/monolog/) logging library 

it provides a simple way to log messages
