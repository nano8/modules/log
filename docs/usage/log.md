---
layout: doc
---

<script setup>
const args = {
    defineLogHandler: [
        {
            name: 'handler',
            type: 'Handler',
        },
    ],
    useLog: [],
};
</script>

##### laylatichy\nano\modules\log\Log

## usage

## <Types fn="defineLogHandler" r="void" :args="args.defineLogHandler" /> {#defineLogHandler}

to learn more about handlers, check [this](https://seldaek.github.io/monolog/doc/02-handlers-formatters-processors.html#handlers)

```php
defineLogHandler(new StreamHandler('php://stdout', Level::Info));
```

## <Types fn="useLog" r="Log" :args="args.useLog" /> {#useLog}

```php
useLog()->info('hello world');
```

