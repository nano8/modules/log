---
layout: home

hero:
    name:    nano/modules/log
    tagline: log module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/log is a simple logging module
    -   title:   easy to use
        details: |
                 nano/modules/log provides a simple way to log messages
---
